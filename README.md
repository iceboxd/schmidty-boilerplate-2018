# Schmidty-Boilerplate

---

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)


Minimal config for rapid development

A balance between absolute minimal and production-ready minimal

---

## Quick Start

```bash

$ npm install

```

```bash

npm start

```

---

### Available Scripts

## `npm start`

Runs a webpack dev server in development mode. Page reloads on edit.

Open [http://localhost:8080](http://localhost:8080)


## `npm run build`

Builds a production app in the `dist` folder.
